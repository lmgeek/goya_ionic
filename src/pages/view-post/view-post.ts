import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MapDirectionsPage } from '../map-directions/map-directions';
import { Api } from '../../providers/Api';
import { FavoritesProvider } from '../../providers/favorites/favorites';

@IonicPage()
@Component({
  selector: 'page-view-post',
  templateUrl: 'view-post.html',
})
export class ViewPostPage {
	public post : object = this.navParams.get("post");
	constructor(
			public navCtrl: NavController,
			public navParams: NavParams,
			public API:Api,
      public favorite:FavoritesProvider
		) {
  	}

  	goLink(link : string){
  		window.open(link, '_system');
  	}

  	howToGo(){
  		this.navCtrl.push(MapDirectionsPage, {post:this.post});
  	}

    setFavorite(){
      this.favorite.setFavorite(this.post);
    }

}
