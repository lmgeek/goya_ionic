import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { ClientProvider } from '../../providers/client/client';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController } from 'ionic-angular';
import { WelcomePage } from '../welcome/welcome';
import { RegisterPage } from '../register/register';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

	formLogin : FormGroup;

	constructor(
	  	public navCtrl: NavController,
	  	public client: ClientProvider,
	  	public formBuilder: FormBuilder,
	  	public alert: AlertController
	  	) {																																																													
		this.formLogin = this.loginMyForm();
	}

  	ionViewDidLoad() {
    	console.log('ionVirewDisLoad LoginPage');																															
  	}

  	private loginMyForm(){
		return this.formBuilder.group({
			password: ['', [Validators.pattern(/^[a-z0-9_-]{6,18}$/)]],
			email: ['', [Validators.required, Validators.email]],
		});
	}

	saveData(){
		// alert(JSON.stringify(this.formLogin.value));
	}

	toRegister(){
		this.navCtrl.push(RegisterPage);
	}

	login(){
		this.navCtrl.push(WelcomePage);
	}
}
