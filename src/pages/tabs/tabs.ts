import { Component } from '@angular/core';

import { FavoritePage } from '../favorite/favorite';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { MenuController } from 'ionic-angular';
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = FavoritePage;
  tab3Root = ContactPage;

  constructor(public menuCtrl: MenuController) {
  	this.menuCtrl.enable(true);
  }
}
