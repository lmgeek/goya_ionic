import { Component } from '@angular/core';
import { IonicPage, NavController, Platform, AlertController } from 'ionic-angular';
import { CityProvider } from '../../providers/city/city';
import { TabsPage } from '../tabs/tabs';
import { LoginPage } from '../login/login';
import { ClientProvider } from '../../providers/client/client';

@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html',
})
export class WelcomePage {
	cities: any;
	citySelected: number;
  constructor(
      public cityProvider:CityProvider,
      public navCtrl: NavController,
      public plt: Platform,
      public alertCtrl: AlertController,
      public clientProv: ClientProvider
    ) {
  	    this.cityProvider.getCities()
  	    .then((res) => {
  	    	this.cities = res;
  	    })
        .catch( () => {
          this.alertCtrl.create({
            title: 'Error',
            subTitle: 'Ha ocurrido un error, verifique su conexion a internet o vuelva a intentarlo mas tarde',
            buttons: ['Ok']
          }).present();
        })
  }

  onChangeCity(){
  	this.cityProvider.setSelected(this.citySelected);
  }

  run(){
  	if(this.citySelected){
  		this.navCtrl.push(TabsPage);
  	}
  }

  toLogin(){
    this.navCtrl.push(LoginPage);
  }

}
