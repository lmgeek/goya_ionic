import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FavoritesProvider } from '../../providers/favorites/favorites';
import { ViewPostPage } from '../view-post/view-post';
import { Api } from '../../providers/Api';
@Component({
  selector: 'page-favorite',
  templateUrl: 'favorite.html'
})
export class FavoritePage {

  constructor(public navCtrl: NavController, public favorites:FavoritesProvider, public API : Api) {

  }

  	onClickCard(post : object){
		this.navCtrl.push(ViewPostPage, {post:post});
	}

}
