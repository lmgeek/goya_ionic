import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
declare var google;

@IonicPage()
@Component({
  selector: 'page-map-directions',
  templateUrl: 'map-directions.html',
})
export class MapDirectionsPage {
	public post : any = this.navParams.get("post");
	Destination: any;
  	MyLocation: any;

  	constructor(
  		public navParams:NavParams,
  		public navCtrl: NavController,
  		public http: HttpClient,
  		public alertCtrl: AlertController
  	) {

  	}

  	ionViewDidLoad(){
	    this.calculateAndDisplayRoute();
	}

	calculateAndDisplayRoute() {
		let that = this;
		let directionsService = new google.maps.DirectionsService;
		let directionsDisplay = new google.maps.DirectionsRenderer;
		const map = new google.maps.Map(document.getElementById('mapDirectionContent'), {
			zoom: 7,
			center: {lat: 11.227138, lng: 74.212447}
		});
		directionsDisplay.setMap(map);

		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {
			    const pos = {
			    	lat: ( typeof position.coords.latitude === "string" ) ? parseFloat(position.coords.latitude) : position.coords.latitude,
			    	lng: ( typeof position.coords.longitude === "string" ) ? parseFloat(position.coords.longitude) : position.coords.longitude
			    };
			    const posDest = {
			    	lat: (typeof that.post.latitud === "string") ? parseFloat(that.post.latitud) : that.post.latitud,
			      	lng: (typeof that.post.longitud === "string") ? parseFloat(that.post.longitud) : that.post.longitud
			    }
			    map.setCenter(pos);
			    alert(JSON.stringify(posDest));
			    directionsService.route({
					origin: pos,
					destination: posDest,
					travelMode: 'DRIVING'
				}, function(response, status) {
						if (status === 'OK') {
						  directionsDisplay.setDirections(response);
						} else {
							that.alertCtrl.create({
						      title: 'Error',
						      subTitle: 'Ha ocurrido un error al cargar el mapa. Verifique que el GPS se encuentra activado y vuelva a abrir la app',
						      buttons: ['OK']
						    }).present();
						  console.log('Directions request failed due to ' + status);
						}
					});

			}, function() {
				that.alertCtrl.create({
			      title: 'Error',
			      subTitle: 'Ha ocurrido un error al cargar el mapa. Verifique que el GPS se encuentra activado y vuelva a abrir la app',
			      buttons: ['OK']
			    }).present();
			});
		} else {
		  // Browser doesn't support Geolocation
		  	that.alertCtrl.create({
			    title: 'Error',
			    subTitle: 'Su dispositivo no nos permite acceder a su ubicacion.',
			    buttons: ['OK']
			}).present();
		}

	}

}
