import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';

import { FavoritePage } from '../pages/favorite/favorite';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { WelcomePage } from '../pages/welcome/welcome';
import { ViewPostPage } from '../pages/view-post/view-post';
import { MapDirectionsPage } from '../pages/map-directions/map-directions';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';

import { Facebook } from "@ionic-native/facebook"; 

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CityProvider } from '../providers/city/city';
import { Api } from '../providers/Api';
import { CategoryProvider } from '../providers/category/category';
import { PostsProvider } from '../providers/posts/posts';

import { NgxQRCodeModule } from 'ngx-qrcode2';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

import { GoogleMaps } from '@ionic-native/google-maps';
import { FavoritesProvider } from '../providers/favorites/favorites';
import { CardsProvider } from '../providers/cards/cards';
import { ClientProvider } from '../providers/client/client';

@NgModule({
  declarations: [
    MyApp,
    FavoritePage,
    ContactPage,
    HomePage,
    TabsPage,
    WelcomePage,
    ViewPostPage,
    MapDirectionsPage,
    LoginPage,
    RegisterPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    NgxQRCodeModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    FavoritePage,
    ContactPage,
    HomePage,
    TabsPage,
    WelcomePage,
    ViewPostPage,
    MapDirectionsPage,
    LoginPage,
    RegisterPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CityProvider,
    Api,
    CategoryProvider,
    PostsProvider,
    BarcodeScanner,
    GoogleMaps,
    FavoritesProvider,
    CardsProvider,
    Facebook,
    Geolocation,
    ClientProvider
  ]
})
export class AppModule {}
