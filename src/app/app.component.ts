import { Component } from '@angular/core';
import { Platform, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
//import { WelcomePage } from '../pages/welcome/welcome';
import { LoginPage } from '../pages/login/login';
//import { RegisterPage } from '../pages/register/register';
import { CategoryProvider } from '../providers/category/category';
import { PostsProvider } from '../providers/posts/posts';
import { CityProvider } from '../providers/city/city';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  //rootPage : any = WelcomePage;
  rootPage : any = LoginPage;
  constructor(
      public menuCtrl: MenuController,
      platform: Platform,
      statusBar: StatusBar,
      splashScreen: SplashScreen,
      public CategoriesProv: CategoryProvider,
      public PostProv : PostsProvider,
      public CityProv : CityProvider,
    ) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
      this.CategoriesProv.query().then( () => {menuCtrl.enable(false);})
      .catch( () => null);
    });
  }

  onClickCategory(id:number){
    this.PostProv.query(id, this.CityProv.selected);
    this.menuCtrl.close();

  }

}
