import { Storage } from '@ionic/storage';
import { Injectable } from '@angular/core';

@Injectable()
export class FavoritesProvider {
	public favorites : [any] = [1];

  	constructor(private storage: Storage) {
    	this.storage.ready().then(() => {
    		this.getFavorites();
    	});
  	}

  	getFavorites(){
  		this.storage.get('favorites')
		.then(val => {
			if(val){
				this.favorites = val;
			}else{
        this.favorites.splice(0,this.favorites.length)
      }
		})
		.catch(err => {
			console.error(err);
			this.favorites.splice(0,this.favorites.length)
		});
  	}

  	setFavorite(post:object){
  		this.favorites.push(post);
  		this.storage.set('favorites', this.favorites);
  	}

}
