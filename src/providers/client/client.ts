import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import {Api} from '../Api';

@Injectable()
export class ClientProvider {

	client:any;

	constructor(public http: HttpClient, public storage:Storage, public Api:Api) {
    	this.storage.ready().then(() => {
    		this.query();
    	});
	}

	query(){
		this.storage.get('client')
		.then( (val) => {
			if(val){
				this.client = JSON.parse(val);
			}
		})
		.catch( err => {
			console.error(err);
			this.client = null;
		})
	}

	requestCreate(email, name, age){
		const url = this.Api.GetURL();
		return new Promise( (resolve, reject) => {
			this.http.get(
				url+"/newCustomer/"+email+"/"+name+"/"+age
				).subscribe((res) => {
					this.client = res;
					this.storage.set('client', JSON.stringify(res))
					.then( () => {
						resolve();
					})
					.catch( err => {
						console.error(err);
					})
				})
				err => {
					console.error(err);
					reject();
				}
			}
		);
	}

}
