export class Api {
	public urlApiRest: string = "http://admin.appgoya.co/api/v1";
	public urlImage : string = "http://admin.appgoya.co/image";
	constructor(){
	}
	
	GetURL() : string{
		return this.urlApiRest;
	}

	GetURLImage() : string{
		return this.urlImage;
	}
}