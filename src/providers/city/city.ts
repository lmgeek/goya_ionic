import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Api } from '../Api';


@Injectable()
export class CityProvider {
	public selected: number;
	public cities: any;
  private url : string = this.API.GetURL();

  constructor(public API : Api, public http: HttpClient) {
    console.log(this.url);
  }

  getCities(){
  	return new Promise((resolve, reject) => {
      this.http.get(this.url + "/getCities").subscribe((res) => {
        this.cities = res;
        resolve(res);
      }, (err) => {
        console.log(err);
        reject();
      });
    });
  }

  setSelected(id:number){
  	this.selected = id;
  }
}
