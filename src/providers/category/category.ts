import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Api } from '../Api';


@Injectable()
export class CategoryProvider {

	public categories : any;
	private url : string = this.API.GetURL();

	constructor(public API : Api, public http: HttpClient) {
		
	}

	query(){
		return new Promise((resolve, reject) => {
			this.http.get(this.url + "/getCategories").subscribe((res) => {
		        this.categories = res;
		        resolve();
		    }, (err) => {
		        console.error(err);
		        reject();
		    });
		});
	}
}
